resource "aws_s3_bucket" "site_bucket" {
  bucket = "cheatsheet-bucket"
  acl = "public-read"
  force_destroy= true
  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["PUT","POST"]
    allowed_origins = ["*"]
    expose_headers = ["ETag"]
    max_age_seconds = 3000
  }
  policy = <<POLICY
{
  "Version":"2012-10-17",
  "Statement":[{
    "Sid":"PublicReadForGetBucketObjects",
        "Effect":"Allow",
      "Principal": "*",
      "Action":"s3:GetObject",
      "Resource":["arn:aws:s3:::cheatsheet-bucket/*"
      ]
    }
  ]
}
POLICY

  tags {
    Trigramme          = "cypa"
    Name        = "cheatsheet-bucket"
  }
  website {
    index_document = "index.html"
    error_document = "404.html"
  }
}

# # AWS S3 bucket for www-redirect
# resource "aws_s3_bucket" "website_redirect" {
#   bucket = "www.cheatsheet-bucket"
#   acl = "public-read"
#
#   website {
#     redirect_all_requests_to = "cheatsheet-bucket"
#   }
# }
#
# resource "aws_route53_record" "route_to_S3" {
#   zone_id = "${var.hosted_public_zone_id}"
#   name    = "${var.website_domain}"
#   type    = "A"
#
#   alias {
#     name                   = "${aws_s3_bucket.site_bucket.website_domain}"
#     zone_id                = "${aws_s3_bucket.site_bucket.hosted_zone_id}"
#     evaluate_target_health = false
#   }
# }

#
# resource "aws_cloudfront_distribution" "cdn" {
#   depends_on = ["aws_s3_bucket.site_bucket"]
#   origin {
#     domain_name = "${aws_s3_bucket.site_bucket.name}.s3.amazonaws.com"
#     origin_id = "site_bucket_origin"
#     s3_origin_config {
#     }
#   }
#   enabled = true
#   default_root_object = "index.html"
#   aliases = ["${aws_s3_bucket.site_bucket.name}"]
#   price_class = "PriceClass_200"
#   retain_on_delete = true
#   default_cache_behavior {
#     allowed_methods = [ "DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT" ]
#     cached_methods = [ "GET", "HEAD" ]
#     target_origin_id = "site_bucket_origin"
#     forwarded_values {
#       query_string = true
#       cookies {
#         forward = "none"
#       }
#     }
#     viewer_protocol_policy = "allow-all"
#     min_ttl = 0
#     default_ttl = 3600
#     max_ttl = 86400
#   }
#   viewer_certificate {
#     cloudfront_default_certificate = true
#   }
#   restrictions {
#     geo_restriction {
#       restriction_type = "none"
#     }
#   }
# }
#
# resource "aws_route53_record" "route53_to_cdn" {
#   zone_id = "${var.hosted_public_zone_id}"
#   name = "${aws_s3_bucket.site_bucket.name}"
#   type = "A"
#
#   alias {
#     name = "${aws_cloudfront_distribution.cdn.domain_name}"
#     zone_id = "${aws_s3_bucket.site_bucket.hosted_zone_id}"
#     evaluate_target_health = false
#   }
# }
