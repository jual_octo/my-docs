# Terraform
## Ticks got if
### Create resource depending on boolean
```
variable "cluster_database" {
  default     = false
  description = "create cluster or unique db"
}

resource "aws_rds_cluster" "default" {
  count                   = "${var.cluster_database ? 1 : 0 }"
```

### schrodinger resources
how to get database url depending on perhaps existing resources
```
data "aws_db_instance" "database" {
  count                   = "${var.cluster_database ? 0 : 1 }"
  db_instance_identifier = "${var.environment}-database"
}

data "aws_rds_cluster" "cluster_database" {
  count                   = "${var.cluster_database ? 1 : 0 }"
  cluster_identifier      = "cluster-${var.application}-${var.environment}"
}

locals {
  //get correct url depending exitance of cluster or rds
  cluster_url = "${element(concat(data.aws_rds_cluster.cluster_database.*.endpoint, data.aws_db_instance.database.*.address),0)}"
}

```


## environment workspace & wrapper
### How to init new environment/workspace
```
cd numengie-infra/terraform_ecs_engie
tf init
tf workspace new dev
```

It will create a _terraform.tfstate.d_ folder inside each folder with your tfstate for each environment



WARNING : Use same name for each *environment*.tfvars and your workspace


### How to select an workspace

To display which workspace you're on

`terraform workspace list`
```
default
* dev
```
To select a different workspace
`
terraform workspace select default
   Switched to workspace "default".
`

### Install a wrapper

Before each of the commands listed below. it will:
- check that you are in the same workspace in all your directories
- check that your environment variables match the workspace you are in

It will also apply your command to all the directories within your current directory

Clone the wrapper [project](https://github.com/sebiwi/terraform-wrapper).

Symlink the `tf.rb` file somewhere in your path, like so:

`ln -s <terraform_wrapper_directory>/tf.rb /usr/local/bin/tf`
Remember to use the full path to your terraform-wrapper’s directory.

Commands will be applied to all directories within current directory:
- tf workspace new <workspace>
- tf workspace select <workspace>
- tf <workspace> plan
- tf <workspace> apply
- tf <workspace> destroy

**WARNING 1**: apply and destroy do not do a plan before applying changes
**WARNING 2**: make sure workspaces and environments <environment>.tfvars share the same name


### Compulsary tags & names:

The resource names must be as clear as possible. Example of desired format :

```
name = "<resource-type>${var.solution}-${var.component}-${var.application}-${var.environment}-additional-details"

```
